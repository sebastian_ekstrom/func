from binary_tests import ex
# There are 10 kinds of people in this world. 
# Those who can read binary and those who can't

# Mark your solutions with @ex(#) where # is the number of the exercise. See
# the example assignment #0 below:

# Assignment 0 
#-------------
# Write a function that takes a number n
# and returns n+1.

@ex(0)
def inc(n):
    return n+1
    
# You can test a single exercise by entering
# grader filename ex.exercisenumber
# in the terminal (for instance: "grader binary.py ex.0" will test the
# example assignment above), or test all the exercises with
# grader filename
   

# Assignment 1 
#-------------
# Write a function that turns a number into binary 
# experiment with the builtin function bin() 
# example num2bin(23) should return '0b10111'

@ex(1)
def num2bin(n):
    pass
    
# Assignment 2 
#-------------
# This function returns True if the number is positive and in the interval 0 - 31
# Change it so that it can take start and stop arguments for any interval
# Make sure that the default value for start == -1 for stop == 31


# Assignment 3
#-------------
# Create a function that takes a number as argument and returns a string of 
# an integer and its binary number format.
# The returned string should have the format:
#
#   "The number 2 is written as 0b10 in binary"
#
# Use verify_range() to make sure that the number is between 0 and 16
# return "Error" if the number is out of range.
# use num2bin() for the convertion


# Assignment 4
#-------------
# Import this module into the interpreter
# Try to write code that prints out the number to binary conversion between 0 and 32 using the while statement
# and the print() function




